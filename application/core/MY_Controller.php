<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

require_once('vendor/autoload.php');

class MY_Controller extends CI_Controller{

    public $role = "";

    public function __construct()
    {

        parent::__construct();

        header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, jwt');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        header("Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, jwt");

        // $this->verif_token();

    }

    //vérification du token pour restriction de certaines zones
    public function verif_token(){

        // var_dump(getallheaders());

        // foreach(getallheaders() as $name => $value){
        //     echo "$name : $value\n";
        // }

        // var_dump($this->router->routes);

        // echo $_SESSION['apiUri'].$_SERVER["REQUEST_URI"]."\n";
        
        // var_dump($headers);

        // récupération de la liste des headers
        $headers = getallheaders();

        //check si dans le header authorization existe
        if(!isset($headers['authorization'])){

            header("HTTP/1.0 401 Unauthorized");
            die;

        }else{

             //récupération clé token qui a permis de générer un token lors de la connexion 
             $key = '<&zZYKLB4.~2<kkCz[)"cO^f}1&2I'.date("d/l/w/z/F/m/L/Y");
            
             try{

                 //si le token dans le header est le même que celui qui a été généré pour l'utilisateur lors de la connexion, on peut décoder ce qui laisse l'accès à un utilisateur aux routes qui lui sont autorisés
                 $decoded = JWT::decode($headers['authorization'], $key, array('HS256'));
                 //le token a été décodé ce qui laisse l'accès aux données de l'utilisateur comme ici récupération du role de l'utilisateur
 
             } catch (Exception $ex) {
 
                 header("HTTP/1.0 401 Unauthorized");
                 die;
 
             }

        }

    }

    //configuration et connection à distance au serveur smtp via google account
    public function send_mail($email){

        //récupération de l'email de l'utilisateur + password dans config json
        $key = json_decode(file_get_contents("../config.json"));

        //Load email library
        $this->load->library('email');

        //SMTP & mail configuration
        $config = array(
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_port' => 465,
            'smtp_user' => $key->gmailUser,
            'smtp_pass' => $key->gmailPassword,
            'mailtype'  => 'html',
            'charset'   => 'utf-8'
        );
        
        //initialisation
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        //Email content
        $htmlContent = '<h1>Paiement Stripe</h1>';
        $htmlContent .= '<p>Le paiement a été un succès</p>';

        $this->email->to($email);
        $this->email->from($key->gmailUser,'Lastcar');
        $this->email->subject('Trajet paiement');
        $this->email->message($htmlContent);

        //Send email
        $this->email->send();

    }

    //système permettant d'upload des images
    public function do_upload(){

        //chemin de l'upload
        $config['upload_path']          = './uploads/';
        //type d'image pouvant être upload
        $config['allowed_types']        = 'gif|jpg|png';
        //poids max de l'image
        $config['max_size']             = 100;
        //taille max largeur de l'image
        $config['max_width']            = 1024;
        //taille max hauteur de l'image
        $config['max_height']           = 768;

        //library upload permettant le fonctionnement du système upload
        $this->load->library('upload', $config);

        //si l'upload ne s'est pas passé il y a erreur!
        if ( ! $this->upload->do_upload('userfile')) {

            $error = array('error' => $this->upload->display_errors());

            return $error;

        }
        //sinon upload 
        else {

            $data = array('upload_data' => $this->upload->data());

            return $data;

        }
        
    }

}