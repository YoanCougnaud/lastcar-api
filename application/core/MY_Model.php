<?php

class MY_Model extends CI_Model{
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    
    public function setProperties($array){
        //boucle sur array, on récupère la clé et valeur de celui ci
        foreach ($array as $key => $value) {
            //on inclut set + $key représentant le nom de clé de l’input envoyé puis on met la première lettre de la valeur de $key en majuscule par exemple (setPassword)
            $method = 'set'.ucfirst($key);
            //si la méthode existe dans l’objet courant c’est à dire dans le model en question
            if(method_exists($this, $method)){
                //on envoie la valeur dans le setter de l'objet
                $this->$method($value);

            }
            
        }
    }

}