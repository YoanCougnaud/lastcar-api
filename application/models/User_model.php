<?php
/*!!!!!!!!!!!!!Modèle des utilisateurs!!!!!!!!!!!!!*/
class User_model extends MY_Model {

protected $id, $lastname, $firstname, $birthday, $city, $email, $phone, $password, $picture, $role_id, $role_name; 

    // Les données envoyé ou récupérer en bdd ne sont pas encore traités dans les setters (à venir)!!!!!!!
    protected function setId($id){

        // if(is_int($id)){

            $this->id = $id;

        // }else{
        //     return "the output needs to be an integer";
        // }
    }

    protected function setLastname($lastname){

        $this->lastname = htmlspecialchars($lastname);

    }

    protected function setFirstname($firstname){

        $this->firstname = htmlspecialchars($firstname);

    }

    protected function setBirthday($birthday){

        // if(is_a($birthday, 'DateTime')){

            $this->birthday = $birthday;

        // }
    }

    protected function setEmail($email){
        //vérifie si la valeur entrée est bien du même format qu'un email
        if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->email = $email;
        }

    }

    protected function setPhone($phone){
        //regex pemettant de savoir si la valeur est bien un numéro de téléphone
        if(!preg_match('/^[0-9]{10}+$/', $phone)) 
        {
            echo "Invalid phone number"; 
        }else{
            $this->phone = $phone;
        }
    }

    protected function setCity($city){

        $this->city = htmlspecialchars($city);

    }

    protected function setPassword($password){
        //clé de cryptage composer d'un sh1 + grain de sel
        $this->password = '54f28v'.sha1($password."1547n").'h1h2';

    }

    protected function setPicture($picture){
   
        $this->picture = $picture;

    }

    protected function setRole_id($role_id){

        $role_id = 2;

        // if(is_int($role_id)){

            $this->role_id = $role_id;

        // }else{
        //     return "the output needs to be an integer";
        // }

    }

    protected function setRole_Name($role_name){

        $this->role_name = htmlspecialchars($role_name);

    }

    //PARTIE ADMIN

    //récupère la liste de tout les utilisateurs inscrits 
    public function getAll(){
        
        $sql = ("SELECT user.id, user.lastname, user.firstname, user.birthday, user.email, user.phone, user.city, user.password, user.picture, user.role_id, role.name as role_name FROM user INNER JOIN role ON role.id = user.role_id WHERE role.name = 'utilisateur' ");
        
        $query = $this->db->query($sql);
        
        $datas = $query->result();

        return $datas;
        // $toReturn = [];
        
        // foreach ($datas as $key => $value) {
        //     $this->setProperties($value);
        //     $toReturn[$key] = get_object_vars($this);
        // }
        
        // return $toReturn;
        
    }

    //récupère un utilisateur par son id
    public function retrieve($id){

        $sql = "SELECT user.id, user.lastname, user.firstname, user.birthday, user.email, user.phone, user.city, user.password, user.picture, user.role_id, role.name as role_name FROM user INNER JOIN role ON role.id = user.role_id WHERE user.id = $id AND role.name = 'utilisateur'";

        $query = $this->db->query($sql);

        $datas = $query->row();

        return $datas;

        // $toReturn = [];
        
        // foreach ($datas as $key => $value) {
        //     $this->setProperties($value);
        //     $toReturn[$key] = get_object_vars($this);
        // }
        
        // return $toReturn;

    }
    
    //PARTIE UTILISATEUR
    //création d'un utilisateur
    public function create($post){

        $this->setProperties($post);
                
        $sql = "INSERT INTO user(id, firstname, lastname, birthday, email, phone, city, picture, password, role_id) VALUES (NULL, '".$this->firstname."', '".$this->lastname."','".$this->birthday."','".$this->email."',".$this->phone.",'".$this->city."','".$this->picture."','".$this->password."',".$this->role_id.")";

        return $this->db->query($sql);

    }

    //mise à jour de l'utilisateur connecté
    public function update($put){

        $this->setProperties($put);
        
        $sql = "UPDATE user SET firstname = '".$this->firstname."', lastname = '".$this->lastname."', birthday = '".$this->birthday."', city = '".$this->city."', email = '".$this->email."', phone = ".$this->phone.", password = '".$this->password."', picture = '".$this->picture."', role_id = ".$this->role_id." WHERE id = ".$this->id.";";

        return $this->db->query($sql);
    }

    //suppréssion du compte via un utilisateur connecté (à revoir)
    public function delete($id){

        $this->setProperties($id);

        $sql = "DELETE FROM user WHERE id = ".$this->id.";";
        
        return $this->db->query($sql);

    }

}