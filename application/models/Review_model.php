<?php

/*!!!!!!!!!!!!!Modèle des trajets!!!!!!!!!!!!!*/
class Review_model extends MY_Model{

    protected $id, $firstname, $lastname, $score, $comment, $user_id, $usertrip, $trip_id;
    
    // Les données envoyé ou récupérer en bdd ne sont pas encore traités dans les setters (à venir)!!!!!!!!!!!
    protected function setId($id){

        $this->id = $id;

    }

    protected function setLastname($lastname){

        $this->lastname = htmlspecialchars($lastname);

    }

    protected function setFirstname($firstname){

        $this->firstname = htmlspecialchars($firstname);

    }

    protected function setScore($score){

        $this->score = $score;

    }

    protected function setComment($comment){

        $this->comment = $comment;

    }

    protected function setUser_id($user_id){

        $this->user_id = $user_id;

    }

    protected function setTrip_id($trip_id){

        $this->trip_id = $trip_id;

    }

    //récupération des inputs permettant d'envoyer en post les valeurs en bdd
    //récupération de user_id de la table trip ayant pour id = id
    //récupération de user_id, firstname, lastname de la table usertrip ayant pour id = id
    //récupération de id de la table trip ayant id = id
    //ajout des résultats récupérés dans sql1,sql2,sq3 puis ajout des résultats dans sql4 pour insertion des données dans la table review en bdd
    public function createReview(){

        $firstname = $review['firstname'];
        $lastname = $review['lastname'];
        $score = $review['score'];
        $comment = $review['comment'];
        $userid = $review['user_id'];
        $tripid = $review['trip_id'];

        $sql1 = "SELECT trip.user_id FROM trip WHERE trip.user_id = $userid";
        $sql2 = "SELECT usertrip.user_id, usertrip.firstname, usertrip.lastname FROM usertrip WHERE usertrip.user_id = $userid";
        $sql3 = "SELECT trip.id FROM trip WHERE trip.id = $tripid";

        $query = $this->db->query($sql1);
        $result = $query->row();

        $query2 = $this->db->query($sql2);
        $result2 = $query2->row();

        $query3 = $this->db->query($sql3);
        $result3 = $query3->row();

        $sql4 = "INSERT INTO review( id, firstname, lastname, score, comment, user_id, usertrip_id, trip_id) VALUES (NULL, '".$firstname."', '".$lastname."', '".$score."', '".$comment."', ".$result->id.", ".$result2->id.", ".$result3->id.")";

        return $this->db->query($sql4);

    }

}