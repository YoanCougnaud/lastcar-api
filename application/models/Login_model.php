<?php

class Login_model extends CI_Model{

    public function __construct(){

        $this->load->database();

    }

    //requête permettant la connexion à un admin sur le site admin
    public function connexionAdmin($array){

        $email = $array['email'];
        // $password = '54f28v'.sha1($array['password']."1547n").'h1h2';
        $password = $array['password'];

        $sql=("SELECT user.id, user.lastname, user.firstname, user.birthday, user.email, user.phone, user.city, user.password, user.picture, role.name FROM user INNER JOIN role ON(user.role_id = role.id) WHERE email ='".$email."' AND password='".$password."'
        AND role.id = 1 ");

        $query = $this->db->query($sql);

        $result = $query->row(); 
            
        if($result != null){

            $data = [
                "id" => $result->id,
                "firstname" => $result->firstname,
                "lastname" => $result->lastname,
                "email" => $result->email,
                "password" => $result->password,
                "role" => $result->name
            ];
            
            return $data;

        }else{

            return false;

        }

    }


    //requête permettant la connexion à un utilisateur sur le site client
    public function connexionSite($array){

        $email = $array['email'];
        $password = '54f28v'.sha1($array['password']."1547n").'h1h2';
        // $password = $array['password'];

        //selection des données liés à l'utilisateur ayant pour email et password qui ont été envoyés dans l'input post email et password et qui coordonne avec ceux en bdd
        $sql=("SELECT user.id, user.lastname, user.firstname, user.birthday, user.email, user.phone, user.city, user.password, user.picture, role.name FROM user INNER JOIN role ON user.role_id = role.id INNER JOIN trip ON user.id = trip.user_id WHERE email ='".$email."' AND password='".$password."'
        AND role.id= 2");

        //validation de la requête sql
        $query = $this->db->query($sql);

        //récupération du résultat lié à cette utilisateur
        $result = $query->row();
            
        //si les résultat ne sont pas null
        if($result != null){

            //tableau associatif comportant les valeurs récupérés en bdd
            $data = [
                "id" => $result->id,
                "firstname" => $result->firstname,
                "lastname" => $result->lastname,
                "city" => $result->city,
                "picture" => $result->picture,
                "birthday" => $result->birthday,
                "email" => $result->email,
                "phone" => $result->phone,
                "password" => $result->password,
                "role" => $result->name
            ];
            
            //renvoie des données sur le controller
            return $data;


        //si le resultat est vide ça renvoie false
        }else{

            return false;

        }

    }

    //requête permettant la connexion à un utilisateur sur le site client
    // public function connexionSite($array){

    //     $email = $array['email'];
    //     // $password = '54f28v'.sha1($array['password']."1547n").'h1h2';
    //     $password = $array['password'];

    //     $sql=("SELECT user.id, user.lastname, user.firstname, user.birthday, user.email, user.phone, user.city, user.password, user.picture, role.name FROM user INNER JOIN role ON user.role_id = role.id INNER JOIN trip ON user.id = trip.user_id WHERE email ='".$email."' AND password='".$password."'
    //     AND role.id= 2");

    //     $query = $this->db->query($sql);

    //     $result = $query->row();
            
    //     if($result != null){

    //         $sql=("SELECT trip.* FROM trip INNER JOIN user ON user.id = trip.user_id WHERE trip.user_id = '".$result->id."'");

    //         $query = $this->db->query($sql);

    //         $tripResult = $query->result();

    //         $data = [
    //             $result,
    //             $tripResult
    //         ];

    //         return $data;

    //     }else{

    //         return false;

    //     }

    // }

}

