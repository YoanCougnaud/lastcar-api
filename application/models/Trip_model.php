<?php

/*!!!!!!!!!!!!!Modèle des trajets!!!!!!!!!!!!!*/
class Trip_model extends MY_Model{

    protected $id, $city_from, $city_to, $prix, $date, $datemax, $pickup, $user_id, $firstname;
    
    // Les données envoyé ou récupérer en bdd ne sont pas encore traités dans les setters (à venir)!!!!!!!!!!!
    protected function setId($id){

        $this->id = $id;

    }

    protected function setCity_from($city_from){

        $this->city_from = $city_from;

    }

    protected function setCity_to($city_to){

        $this->city_to = $city_to;

    }

    protected function setDate($date){

        $this->date = $date;

    }

    //récupère l'heure en format datetime via le site client puis change l'heure et minute récupéré avec la date maxi de la journée qui est 23:59:00
    protected function setDatemax($datemax){
        //sépare la date et les heures minutes seconde en tableau de deux index
        $s = explode(" ",$datemax);
        //retire l'index 1 heure, minutes et secondes
        unset($s[1]);
        //ajoute 23:59:00 à l'index 1
        $s[1] = "23:59:00";
        //retransforme le tableau en simple string
        $datemax = implode(" ",$s);
        //ajout de la valeur de la propriété 
        $this->datemax = $datemax;

    }

    protected function setPickup($pickup){

        $this->pickup = htmlspecialchars($pickup);

    }

    protected function setPrix($prix){

        $price = $prix*100;

        $this->prix = $price;

    }

    protected function setUser_id($user_id){

        $this->user_id = $user_id;

    }

    protected function setFirstname($firstname){

        $this->firstname = htmlspecialchars($firstname);

    }

/*
    partie admin
*/
    //récupère tout les trajets des utilisateurs inscrits sur le site client et affiche la liste dans la partie admin
    public function getAll(){

        $sql = "SELECT trip.* From trip INNER JOIN user ON user.id = trip.user_id WHERE user.role_id = 2";

        $query = $this->db->query($sql);

        return $query->result();

    }

    //récupère le trajet via son id
    public function retrieve($id){

        $sql = "SELECT trip.* FROM trip WHERE trip.id = $id";

        $query = $this->db->query($sql);

        $datas = $query->row();

        return $datas;

    }

    //récupère l'id d'un trajet et récupère l'id, firstname, lastname d'un utilisateur et les résultats sont insérés dans la table usertrip avec un Insert into
    public function userTrip($dataUser){

        $firstname = $dataUser['firstname'];
        $lastname = $dataUser['lastname'];
        $tripid = $dataUser['trip_id'];
        $userid = $dataUser['user_id'];

        $sql = "SELECT trip.id, city_from, city_to, prix, date FROM trip WHERE trip.id = $tripid";
        $sql2 = "SELECT user.id, user.firstname, user.lastname FROM user WHERE user.id = $userid";

        $query = $this->db->query($sql);
        $result = $query->row();

        $query2 = $this->db->query($sql2);
        $result2 = $query2->row();

        $sql3 = "INSERT INTO usertrip( id, firstname, lastname, city_from, city_to, date, price, user_id, trip_id) VALUES (NULL, '".$firstname."', '".$lastname."','".$result->city_from."','".$result->city_to."','".$result->date."',".$result->prix.",".$result2->id.", ".$result->id.")";

        return $this->db->query($sql3);

    }

    //Je dois récupérer tout les trajets correspondant au conducteur et au passager ayant le même id
    public function getAllTripByUserId($id){

        $sql = "SELECT trip.* FROM trip INNER JOIN user ON user.id = trip.user_id WHERE trip.user_id = $id";

        $sql2 = "SELECT usertrip.* FROM usertrip INNER JOIN user ON user.id = usertrip.user_id WHERE usertrip.user_id = $id";

        $query = $this->db->query($sql);

        $query2 = $this->db->query($sql2);

        $data = $query->result();

        $data2 = $query2->result();

        $userListTrip = [
            "conducteur" => $data,
            "passager" => $data2
        ];

        return $userListTrip;

    }

    //récupère le trajet id de l'utilisateur
    public function getTripIdByUserId($idUser,$idTrip){

        $sql = "SELECT trip.*, user.id as userId From trip INNER JOIN user ON user.id = trip.user_id WHERE user.id = $idUser AND trip.id = $idTrip";

        $query = $this->db->query($sql);

        return $query->row();

    }

    // récupère tout les utilisateurs ayant participé à un trajet 
    public function getUsersByTripId($id){

        $sql = "SELECT user.* From trip INNER JOIN user ON user.id = trip.user_id WHERE trip.id = $id AND user.role_id = 2";

        $query = $this->db->query($sql);

        return $query->result();

    }

    //récupère un utilisateur ayant participé au trajet id
    public function getUserIdByTripId($tripId,$userId){

        $sql = "SELECT user.* From trip INNER JOIN user ON user.id = trip.user_id WHERE trip.id = $tripId AND user.id = $userId AND user.role_id = 2";

        $query = $this->db->query($sql);

        return $query->result();

    }

/*
    partie utilisateur
*/
    // (barre de recherche) récupère tout les trajets entre une ville de départ et celle d'arrivée d'une date précise jusqu'à minuit de la date sélectionné
    public function getAllTripByCity($city_from,$city_to,$date){
        
        $datemax = $date;

        $this->setCity_from($city_from);

        $this->setCity_to($city_to);

        $this->setDate($date);
        
        $this->setDatemax($datemax);

        $sql = "SELECT trip.*, user.firstname, user.lastname, user.birthday, user.picture FROM trip INNER JOIN user ON user.id = trip.user_id WHERE city_from LIKE '$this->city_from' AND city_to LIKE '$this->city_to' AND date BETWEEN '$this->date' AND '$this->datemax' ORDER BY date ASC";

        $query = $this->db->query($sql);

        $datas = $query->result();  

        $toReturn = [];
        
        foreach ($datas as $key => $value) {
            $this->setProperties($value);
            $toReturn[$key] = get_object_vars($this);
        }
        
        return $toReturn;
        
    }

    //récupère le trajet id de la liste des trajets récupéré via getAllTripByCity
    public function getTripIdByCity($id){

        $sql = "SELECT trip.*, user.firstname, user.lastname, user.birthday, user.picture FROM trip INNER JOIN user ON user.id = trip.user_id WHERE trip.id = $id";

        $query = $this->db->query($sql);

        $datas = $query->row();

        return $datas;

    }

    //requête permettant à un utilisateur connecté de créer un trajet
    public function createTrip($post){
        
        $this->setProperties($post);

        $sql = "INSERT INTO trip(id, city_from, city_to, date, prix, pickup, user_id) VALUES (NULL, '".$this->city_from."', '".$this->city_to."','".$this->date."',".$this->prix.",'".$this->pickup."', ".$this->user_id.")";

        return $this->db->query($sql);
        
    }

    //requête permettant à un utilisateur connecté de mettre à jour un trajet
    public function updateTrip($put){

        $this->setProperties($put);

        $sql = "UPDATE trip SET city_from = '".$this->city_from."', city_to = '".$this->city_to."', prix = ".$this->prix.", date = '".$this->date."', pickup = '".$this->pickup."', user_id = ".$this->user_id." WHERE id = ".$this->id.";";

        return $this->db->query($sql);
    }

    //supprime un trajet (à revoir) requête voulu : permettant à un utilisateur connecté de supprimer un trajet
    public function deleteTrip($id){

        $this->setProperties($id);

        $sql = "DELETE FROM trip WHERE id = ".$this->id.";";
        
        return $this->db->query($sql);

    }
}