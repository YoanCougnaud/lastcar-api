<?php
/*!!!!!!!!!!!Modèle Evènement!!!!!!!!!!!!*/
class Event_model extends MY_Model{

    protected $id, $name, $description, $date, $user_id;

    // Les données envoyé ou récupérer en bdd ne sont pas encore traités dans les setters (à venir)!!!!!!!!!!!

    protected function setId($id){

        $this->id = $id;

    }

    protected function setName($name){

        $this->name = $name;

    }

    protected function setDescription($description){

        $this->description = $description;

    }

    protected function setDate($date){

        $this->date = $date;

    }

    protected function setUser_id($user_id){

        $this->user_id = $user_id;

    }

/*
    partie admin uniquement
*/

    //récupération de tout les évènements
    public function getAll(){

        $sql = "SELECT event.*, user.id as userId From event INNER JOIN user ON user.id = event.user_id WHERE user.role_id = 1";

        $query = $this->db->query($sql);

        return $query->result();

    }

    //récupération d'un évènement par son id
    public function retrieve($id){

        $sql = "SELECT event.id, event.name, event.description, event.date FROM event WHERE event.id = $id";

        $query = $this->db->query($sql);

        $datas = $query->row();

        return $datas;

    }


    // public function getEventIdByUserId($idUser,$idEvent){

    //     $sql = "SELECT event.*, user.id as userId From event INNER JOIN user ON user.id = event.user_id WHERE user.id = $idUser AND event.id = $idEvent";

    //     $query = $this->db->query($sql);

    //     return $query->row();
        
    // }

    //création d'un évènement par un admin uniquement
    public function createEvent($post){

        $name = $post['name']; 
        $description = $post['description']; 
        $date = $post['date']; 
        $user_id = $post['user_id'];
        // $this->setProperties($post);

        $sql = "INSERT INTO event(id, name, description, date, user_id) VALUES (NULL, '".$name."', '".$description."','".$date."',".$user_id.")";

        return $this->db->query($sql);

    }

    //mise à jour d'un évènement par un admin uniquement
    public function updateEvent($put){

        $id = $put['id'];
        $name = $put['name']; 
        $description = $put['description']; 
        $date = $put['date']; 
        $user_id = $put['user_id'];
        // $this->setProperties($put);

        // $sql = "UPDATE event SET name = '".$this->name."', description = '".$this->description."', date = '".$this->date."', user_id = ".$this->user_id." WHERE id = ".$this->id.";";

        $sql = "UPDATE event SET name = '".$name."', description = '".$description."', date = '".$date."', user_id = ".$user_id." WHERE id = ".$id.";";

        return $this->db->query($sql);
        
    }

    //suppréssion d'un évènement par un admin uniquement
    public function deleteEvent($id){

        $this->setProperties($id);

        $sql = "DELETE FROM event WHERE id = ".$this->id.";";
        
        return $this->db->query($sql);

    }

}