<?php

/*!!!!!!!!!!!!!Modèle des paiements!!!!!!!!!!!!!*/
class Payment_model extends MY_Model{

    protected $id, $charge_id, $customer_id, $firstname, $lastname, $city_from, $city_to, $amount, $currency, $status, $user_id, $trip_id;

    // Les données envoyé ou récupérer en bdd ne sont pas encore traités dans les setters (à venir)!!!!!!!!!!!
    protected function setId($id){

        $this->id = $id;

    }

    protected function setCharge_id($charge_id){

        $this->charge_id = $charge_id;

    }

    protected function setCustomer_id($customer_id){

        $this->customer_id = $customer_id;

    }

    protected function setFirstname($firstname){

        $this->firstname = $firstname;

    }

    protected function setLastname($lastname){

        $this->lastname = $lastname;

    }

    protected function setCity_from($city_from){

        $this->city_from = $city_from;

    }

    protected function setCity_to($city_to){

        $this->city_to = $city_to;

    }

    protected function setAmount($amount){

        $this->amount = $amount;

    }

    protected function setCurrency($currency){

        $this->currency = $currency;

    }

    protected function setStatus($status){

        $this->status = $status;

    }

    protected function setUser_id($user_id){

        $this->user_id = $user_id;

    }

    protected function setTrip_id($trip_id){

        $this->trip_id = $trip_id;

    }

    //récupère toutes les clés et valeurs envoyés via stripe lors du paiement et stock les infos en bdd que le paiement soit un succès ou a échoué 
    public function transaction($transaction){

        $charge_id = $transaction['charge_id'];
        $customer_id = $transaction['customer_id'];
        $firstname = $transaction['firstname'];
        $lastname = $transaction['lastname'];
        $from = $transaction['city_from'];
        $to = $transaction['city_to'];
        $amount = $transaction['amount'];
        $currency = $transaction['currency'];
        // $price = $prix*100;
        $status = $transaction['status'];
        $userid = $transaction['user_id'];
        $tripid = $transaction['trip_id'];

        $this->setProperties($transaction);

        $sql = "INSERT INTO transaction(id, charge_id, customer_id, firstname, lastname, city_from, city_to, amount, currency, status, user_id, trip_id) VALUES (NULL, '".$charge_id."', '".$customer_id."', '".$firstname."', '".$lastname."', '".$from."', '".$to."',".$amount.", '".$currency."', '".$status."', ".$userid.", ".$tripid.")";

        // $sql = "INSERT INTO transaction(id, charge_id, customer_id, firstname, lastname, city_from, city_to, amount, currency, status, user_id, trip_id) VALUES (NULL, '".$this->charge_id."', '".$this->customer_id."', '".$this->firstname."', '".$this->lastname."', '".$this->from."', '".$this->to."',".$this->amount.", '".$this->currency."', '".$this->status."', ".$this->userid.", ".$this->tripid.")";


        return $this->db->query($sql);

    }

    //permet la récupération de tout les paiements appartenant à un utilisateur par son id
    public function getAllTransactionByUserId($user_id){

        $sql = "SELECT city_from, city_to, amount, currency, status FROM transaction WHERE user_id = $user_id"; 

        $query = $this->db->query($sql);
        $data = $query->result();

        return $data;

    }

}