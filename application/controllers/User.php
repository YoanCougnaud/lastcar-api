<?php

class User extends MY_Controller{

    //ADMIN
    //récupère tout les utilisateurs et envoie les données en json pour affichage de la liste dans la partie admin
    public function getall(){

        $this->verif_token();

        $data = $this->user_model->getall();

        // $this->load->view('list.php', $data);

        header('Content-Type: application/json');

        echo json_encode($data);

    }

    //récupère un utilisateur et envoie les données en json pour affichage des infos concernant l'utilisateur dans la partie admin
    public function retrieve($id){

        $this->verif_token();

        $data = $this->user_model->retrieve($id);

        // $this->load->view('listid.php', $data);

        header('Content-Type: application/json');

        echo json_encode($data);

    }  

    //envoie les clés et valeur lors de l'inscription puis insertion des infos liés à un utilisateur dans la table user par requête sql dans le modèle user 
    public function create(){

        $post = $this->input->post();

        $data = $this->user_model->create($post);

        header('Content-Type: application/json');

        echo json_encode($data);

    }

    //envoie les clés et valeur lors dans la partie profile puis mise à jour des infos liés à un utilisateur connecté dans la table user par requête sql dans le modèle user
    public function update(){

        $this->verif_token();

        $put = $this->input->input_stream();

        $data = $this->user_model->update($put);

        header('Content-Type: application/json');

        echo json_encode($data);

    }

    //suppréssion d'un utilisateur
    public function delete(){

        $this->verif_token();

        $delete = $this->input->post("id",TRUE);

        $this->user_model->delete($delete);

    }

    //permet la récupération des trajet par l'id d'un utilisateur et est affiché dans la partie admin
    public function getAllTripByUserId($id){

        $this->verif_token();
        
        $data = $this->trip_model->getAllTripByUserId($id);

        header('Content-Type: application/json');

        echo json_encode($data);

    }

    //permet la récupération d'un trajet id par l'id d'un utilisateur et est affiché dans la partie admin
    public function getTripIdByUserId($idUser, $idTrip){

        $this->verif_token();

        $data = $this->trip_model->getTripIdByUserId($idUser,$idTrip);

        header('Content-Type: application/json');

        echo json_encode($data);

    }

    // public function getAllEventsByUserId($id){
        
    //     $data = $this->event_model->getAllEventsByUserId($id);
        
    //     header('Content-Type: application/json');
        
    //     echo json_encode($data);
        
        
    // }
    
    // public function getEventIdByUserId($idUser,$idEvent){

    //     $data = $this->event_model->getEventIdByUserId($idUser,$idEvent);
        
    //     header('Content-Type: application/json');
        
    //     echo json_encode($data);
        
    // }

    //methode permettant de récupérer les paiements effectué par un utilisateur dans le model via requête sql puis renvoyé en json dans le controller
    public function getAllTransactionByUserId($id){

        $this->verif_token();
        
        $data = $this->payment_model->getAllTransactionByUserId($id);
        
        header('Content-Type: application/json');
        
        echo json_encode($data);
        
    }

    // public function getTransactionIdByUserId($transaction_id,$user_id){
        
    //     $data = $this->payment_model->getTransactionIdByUserId($transaction_id,$user_id);
        
    //     header('Content-Type: application/json');
        
    //     echo json_encode($data);
        
    // }

    //utilisateur

    //permet d'envoyer les inputs d'un trajet dans le modèle trip pour insertion des données en bdd
    public function createTrip(){

        $this->verif_token();

        $post = $this->input->post();

        $data = $this->trip_model->createTrip($post);

        header('Content-Type: application/json');

        echo json_encode($data);

    }

    //permet d'envoyer les inputs d'un trajet dans le modèle trip pour mise à jour des données en bdd
    public function updateTrip(){

        $this->verif_token();
        
        $put = $this->input->input_stream();

        $data = $this->trip_model->updateTrip($put);

        header('Content-Type: application/json');

        echo json_encode($data);

    }

}