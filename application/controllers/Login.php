<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Login extends CI_Controller{

    public function preflightAcces(){

        $authorised = ["http://lastcar-site.bwb","http://lastcar-api.bwb","http://lastcar-admin.bwb"];

        if($_SERVER['REQUEST_METHOD'] == "OPTIONS") {

            $month = 60*60*24*31;
            
            if(in_array($_SERVER['HTTP_ORIGIN'], $authorised)) {
                echo "true";
                header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_ORIGIN']);
                header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
                header('Access-Control-Allow-Headers: authorization');
                header('Access-Control-Max-Age: '.$month);
                header("Content-Length: 0");
                header("Content-Type: application/json");
            } else {
                header("HTTP/1.1 403 Access Forbidden");
                header("Content-Type: text/plain");
                echo "Vous n'etes pas autorisé à accéder à cette requete";
            }
        }
    }

    //envoie les infos liés au login lors de la connexion : email et password puis récupère les données si les données correspondent à celle en bdd. si les données récupérés email et password match, un token est alors généré puis envoyé en json
    public function connexionAdmin(){

        // var_dump($this->router->routes);

        // var_dump($_SERVER['HTTP_ORIGIN']);

        // var_dump($_SERVER['REMOTE_ADDR']);

        // var_dump($_SERVER["REQUEST_METHOD"]);

        //récupération des input 
        $post = $this->input->post();

        //envoie de l'input post en paramètre dans login modèle puis récupération des données via sql dans le modèle  
        $data = $this->login_model->connexionAdmin($post);

        //affichage json 
        header('Content-Type: application/json');

         //si les input email et password ne sont pas vide on rentre dans les conditions
        if(!empty($this->input->post('password')) && !empty($this->input->post('email'))){

            //si la valeur rentré dans input post password et input post email correspondent pas à celle récupérés en bdd
            if($this->input->post('password') != $data["password"] || $this->input->post('email') != $data["email"]){
    
                //affichage mauvais identifiant
                echo 'mauvais identifiant';
                
            //sinon on génére un token via jwt token
            }else{
                
                //clé du token 
                $key = '<&zZYKLB4.~2<kkCz[)"cO^f}1&2I'.date("d/l/w/z/F/m/L/Y");
                
                //création du token via encodage puis stocké dans la variable data['jwt'] avec pour clé jwt
                $data['jwt'] = JWT::encode($data, $key, "HS256");
    
                //envoie des données lié à l'utilisateur + token généré en json
                echo json_encode($data);
    
            }

        }else{

            echo "inputs vides";

        }
            
    }
    
    //envoie les infos liés au login lors de la connexion : email et password puis récupère les données si les données correspondent à celle en bdd. si les données récupérés email et password match, un token est alors généré puis envoyé en json
    public function connexionSite(){

        //récupération des input 
        $post = $this->input->post();

        //envoie de l'input post en paramètre dans login modèle puis récupération des données via sql dans le modèle  
        $data = $this->login_model->connexionSite($post);

        //affichage json 
        header('Content-Type: application/json');

        //si les input email et password ne sont pas vide on rentre dans les conditions
        if(!empty($this->input->post('password')) && !empty($this->input->post('email'))){

            //si la valeur rentré dans input post password et input post email correspondent pas à celle récupérés en bdd
            if('54f28v'.sha1($this->input->post('password')."1547n").'h1h2' != $data["password"] || $this->input->post('email') != $data["email"]){

                //affichage mauvais identifiant
                echo 'mauvais identifiant';
                
            //sinon on génére un token via jwt token
            }else{
                
                //clé du token 
                $key = '<&zZYKLB4.~2<kkCz[)"cO^f}1&2I'.date("d/l/w/z/F/m/L/Y");
                
                //création du token via encodage puis stocké dans la variable data['jwt'] avec pour clé jwt
                $data['jwt'] = JWT::encode($data, $key, "HS256");
                
                //envoie des données lié à l'utilisateur + token généré en json
                echo json_encode($data);

            }
        
        }

    }

}