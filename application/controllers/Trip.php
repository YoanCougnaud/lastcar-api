<?php

class Trip extends MY_Controller{

    //récupération de tout les trajets, puis envoie des résultats en json
    public function getAll(){

        $this->verif_token();

        $data = $this->trip_model->getall();

        header('Content-Type: application/json');

        echo json_encode($data);

    }

    //récupération d'un trajet puis, envoie des résultats en json
    public function retrieve($id){

        $this->verif_token();

        $data = $this->trip_model->retrieve($id);

        header('Content-Type: application/json');

        echo json_encode($data);

    }

    //récupération de tout les trajet via un formulaire pour la recherche des trajets côté front
    public function getAllTripByCity(){

        $cityFrom = $this->input->post('city_from');
        $cityTo = $this->input->post('city_to');
        $date = $this->input->post('date');       

        $data = $this->trip_model->getAllTripByCity($cityFrom,$cityTo,$date,$pickup);

        header('Content-Type: application/json');

        echo json_encode($data);

    }

    //récupération d'un trajet id affiché via un formulaire puis envoie résultat en json
    public function getTripIdByCity($id){

        //récupération du résultat par id de la requête getTripIdByCity dans le modèle des trajets
        $data = $this->trip_model->getTripIdByCity($id);

        //permet l'affichage du résultat en json pour plus de visibilité sur postman ou sur le navigateur
        header('Content-Type: application/json');

        //résultat encodé en json pour récupération des données vers une partie extérieur
        echo json_encode($data);        

    }

    //permet de récupérer tout les utilisateurs ayant participé au trajet puis envoie résultat en json
    public function getUsersByTripId($id){

        $this->verif_token();

        //récupération du résultat par id de la requête getUsersByTripId dans le modèle des trajets
        $data = $this->trip_model->getUsersByTripId($id);

        header('Content-Type: application/json');

        //résultat encodé en json pour récupération des données vers une partie extérieur
        echo json_encode($data);
        
    }

    //permet de récupérer un utilisateur ayant participé au trajet puis envoie résultat en json
    public function getUserIdByTripId($tripId,$userId){

        $this->verif_token();

        $data = $this->trip_model->getUserIdByTripId($tripId,$userId);

        header('Content-Type: application/json');

        echo json_encode($data);

    }

}