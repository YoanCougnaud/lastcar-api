<?php

class Event extends MY_Controller{

    // public function getAllEventByCityOrName(){

    //     var_dump($_SERVER['REQUEST_URI']); 

    //     $get = $this->input->post('name');

    //     $data = $this->event_model->getAllEventByCityOrName($get);

    //     header('Content-Type: application/json');

    //     echo json_encode($data);

    // }

    //récupère tout les événements sont envoyés en json et affichés dans la partie admin
    public function getAll(){

        $data = $this->event_model->getall();

        header('Content-Type: application/json');

        echo json_encode($data);

    }
    
    //récupère un événements qui est envoyés en json et affichés dans la partie admin
    public function retrieve($id){

        $data = $this->event_model->retrieve($id);

        header('Content-Type: application/json');

        echo json_encode($data);

    }

    //création d'un événement avec l'envoie des clés et valeur via un formulaire puis insertion des données en bdd via requête sql dans le modèle event
    public function create(){

        $post = $this->input->post();

        $data = $this->event_model->createEvent($post);

        header('Content-Type: application/json');

        echo json_encode($data);

    }

    //mise à jour d'un événement avec l'envoie des clés et valeur via un formulaire puis mise à jour des données en bdd via requête sql dans le modèle event
    public function update(){

        $put = $this->input->input_stream();

        $data = $this->event_model->updateEvent($put);

        header('Content-Type: application/json');

        echo json_encode($data);

    }

}