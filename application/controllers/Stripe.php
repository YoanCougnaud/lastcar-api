<?php
require_once('vendor/autoload.php');

class Stripe extends MY_Controller{

    public function __construct() {

        parent::__construct();
        $this->load->library("session");
        $this->load->helper('url');

    }

    //récupère les infos du trajet id vers le trip model et renvoi les données en json
    public function index($id){

        $this->verif_token();

        $data = $this->trip_model->retrieve($id);

        header('Content-Type: application/json');

        echo json_encode($data);

    }

    //sytème de paiement liés à stripe 
    public function payment($id){

        $this->verif_token();

        $json = json_decode(file_get_contents("../config.json"));

        // require_once('application/libraries/stripe-php/init.php');
        
        //clé stripe généré sur le site stripe 
        $stripeSecret = $json->stripeKey;

        //permet d'utiliser la librairie stripe puis ajout de la clé pour identification de l'utilisateur utilisant stripe
        \Stripe\Stripe::setApiKey($stripeSecret);

        //récupération des données du trajet id pour lequel il y aura paiement
        $data = $this->trip_model->retrieve($id);

        //récupère les clé et valeur des inputs contenant les infos de l'utilisateur qui va effectuer le futur paiement et l'id du trajet pour lequel le paiement va être fait 
        $dataUser = $this->input->post();

        //récupération de la clé et valeur de l'input email de l'utilisateur effectuant le paiement 
        $email = $dataUser['email'];

        //récupération de la clé et valeur de l'id du token généré lors du paiement  
        $token = $this->input->post('stripeToken');

        //création d'un client via l'email et l'id du token généré lors du paiement
        $customer = \Stripe\Customer::create ([
            
            "email" => $email,
            "source" => $token

        ]);

        //puis création de la transaction via les infos amount, currency, description et l'id du client
        $charge = \Stripe\Charge::create ([
            
            "amount" => $data->prix,
            "currency" => "eur",
            "description" => "Intro To Payment",
            "customer" => $customer->id

        ]);

        //récupération des infos liés à la transaction puis passé en paramètre en dessou dans le model du paiement pour être envoyé en bdd
        $transaction = [
            'charge_id' => $charge->id,
            'customer_id' => $charge->customer,
            'firstname' => $dataUser['firstname'],
            'lastname' => $dataUser['lastname'],
            'city_from' => $data->city_from,
            'city_to' => $data->city_to,
            'amount' => $charge->amount,
            'currency' => $charge->currency,
            'status' => $charge->status,
            'user_id' => $dataUser['user_id'],
            'trip_id' => $dataUser['trip_id']
        ];

        //récupération des données liés au paiement 
        $createinvoice = $this->payment_model->transaction($transaction);

        // $this->payment_model->transaction($transaction);

        // after successfull payment, you can store payment related information into your database    
        $data = array('success' => true, 'data'=> $charge, 'usertrip' => $dataUser);

        $data2 = [
            "stripe" => $data,
            "facture" => $createinvoice
        ];

        echo json_encode($data2);
        
        //si le paiement a été un succès 
        if($data['success'] == true ){

            // envoie des données liés à l'utilisateur participant au trajet pour lequel il y a eu paiement 
            $data = $this->trip_model->userTrip($dataUser);

            //si la requête en post à été un succès et bien stocké en bdd 
            if($data){

                //envoie du mail automatique liés au client effectuant le paiement
                $this->send_mail($email);

            }

        }

    }

}