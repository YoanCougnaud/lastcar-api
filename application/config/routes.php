<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
// $route['translate_uri_dashes'] = FALSE;
$route['.*']['options'] = 'login/preflightAcces';

$route['login']['post'] = 'login/connexionAdmin';
$route['connexion']['post'] = 'login/connexionSite';

$route['users'] = 'user/getall';
$route['user/(:num)'] = 'user/retrieve/$1';
$route['user']['put'] = 'user/update';
$route['user']['post'] = 'user/create';
$route['user']['delete'] = 'user/delete';

$route['user/(:num)/trips'] = 'user/getAllTripByUserId/$1';
$route['user/(:num)/trip/(:num)'] = 'user/getTripIdByUserId/$1/$2';

$route['user/(:num)/payments'] = 'user/getAllTransactionByUserId/$1';
// $route['user/(:num)/payment/(:num)'] = 'user/getTransactionIdByUserId/$1/$2';

$route['searchtrips']['post'] = 'trip/getAllTripByCity';
$route['searchtrip/(:num)'] = 'trip/getTripIdByCity/$1';

$route['trips'] = 'trip/getall';
$route['trip/(:num)'] = 'trip/retrieve/$1';
$route['trip']['put'] = 'user/updateTrip';
$route['trip']['post'] = 'user/createTrip';

$route['trip/(:num)/users'] = 'trip/getUsersByTripId/$1';
$route['trip/(:num)/user/(:num)'] = 'trip/getUserIdByTripId/$1/$2';

$route['events'] = 'event/getAll';
$route['event/(:num)'] = 'event/retrieve/$1';
$route['event']['put'] = 'event/update';
$route['event']['post'] = 'event/create';
// $route['searchevent'] = 'event/getAllEventByCityOrName';

$route['paiement/(:num)'] = 'stripe/index/$1';
$route['charge/(:num)']['post'] = 'stripe/payment/$1';

$route['email']['post'] = 'email/send_mail';

$route['default_controller'] = 'welcome';